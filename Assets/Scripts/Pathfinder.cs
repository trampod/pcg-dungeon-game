using DataStructures.PriorityQueue;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pathfinder : MonoBehaviour
{
    [SerializeField]
    public DungeonGenerator Generator;

    [Header("Tile Costs")]
    public float EmptyCost;
    public float HallCost;
    public float DoorCost;
    public float RoomCost;

    public List<Vector3Int> CreatePath(RoomTemplate start, RoomTemplate end)
    {
        List<Node> startNodes = new List<Node>();
        List<Vector3Int> endNodes = end.GetDoorTiles();
        List<Vector3Int> doorPositions = start.GetDoorTiles();

        for(int i = 0; i < doorPositions.Count; i++)
        {
            startNodes.Add(new Node(doorPositions[i], 0));
        }
        //for(int i = 0; i < end.DoorPositions.Count; i++)
        //{
        //    endNodes.Add(end.DoorPositions[i]);
        //}

        return Astar(startNodes, endNodes);
    }

    private List<Vector3Int> Astar(List<Node> startNodes, List<Vector3Int> endNodes)
    {
        int queueCount = 0;
        HashSet<Vector3Int> visited = new HashSet<Vector3Int>();
        PriorityQueue<Node, float> queue = new PriorityQueue<Node, float>(0);
        foreach(Node node in startNodes)
        {
            queue.Insert(node, Heuristic(node.Position,endNodes));
            queueCount++;
        }

        Node finalNode = null;

        while(queueCount > 0)
        {
            Node node = queue.Pop();
            queueCount--;
            while(visited.Contains(node.Position))
            {
                node = queue.Pop();
                queueCount--;
            }
            visited.Add(node.Position);

            if(endNodes.Contains(node.Position))
            {
                finalNode = node;
                break;
            }

            List<Vector3Int> neighbours = Neighbours(node);

            foreach (Vector3Int item in neighbours)
            {
                float cost = node.Cost;
                DungeonTile tile = Generator.DungeonGrid[item.x,item.y,item.z];
                switch (tile)
                {
                    case DungeonTile.Empty:
                    case DungeonTile.Reserved:
                        cost += EmptyCost;
                        break;
                    case DungeonTile.Hall:
                        cost += HallCost;
                        break;
                    case DungeonTile.Room:
                    case DungeonTile.Entrance:
                    case DungeonTile.Escape:
                    case DungeonTile.Prop:
                        cost += RoomCost;
                        break;
                    case DungeonTile.Door:
                        cost += DoorCost;
                        break;
                    default:
                        break;
                }
                queue.Insert(new Node(item, cost, node), cost + Heuristic(item, endNodes));
                queueCount++;
            }
        }



        List<Vector3Int> path = new List<Vector3Int>();

        while(finalNode != null)
        {
            path.Add(finalNode.Position);
            finalNode = finalNode.Parent;
        }

        return path;
    }

    private List<Vector3Int> Neighbours (Node node, bool ignoreParent = true)
    {
        List<Vector3Int> result = new List<Vector3Int> ();

        result.Add(node.Position + Vector3Int.forward);
        result.Add(node.Position + Vector3Int.back);
        result.Add(node.Position + Vector3Int.right);
        result.Add(node.Position + Vector3Int.left);

        for(int i = result.Count-1; i >= 0; i--)
        {
            //Debug.Log($"removing {result[i]}\n  ignoreParent={ignoreParent}, node.Parent={node.Parent}\n  DungeonSize={Generator.DungeonSize}");
            if ((ignoreParent && node.Parent != null && node.Parent.Position == result[i]) ||
                result[i].x < 0 ||
                result[i].z < 0 ||
                result[i].x >= Generator.DungeonSize.x ||
                result[i].z >= Generator.DungeonSize.z ||
                Generator.DungeonGrid[result[i].x, result[i].y, result[i].z] == DungeonTile.Room)
            {
                result.RemoveAt(i);
            }
        }

        return result;
    }

    private float Heuristic(Vector3Int position, List<Vector3Int> endNodes)
    {
        float result = float.MaxValue;
        for (int i = 0; i < endNodes.Count; i++)
        {
            float dist = Vector3Int.Distance(position, endNodes[i]);
            if(dist < result)
                result = dist;
        }
        return result;
    }

    private class Node
    {
        public Vector3Int Position;
        public float Cost;
        public Node Parent;

        public Node(Vector3Int position, float cost, Node parent = null)
        {
            Position = position;
            Cost = cost;
            Parent = parent;
        }
    }
}
