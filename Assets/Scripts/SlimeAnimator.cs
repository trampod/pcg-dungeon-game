using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeAnimator : MonoBehaviour
{
    public Vector3 baseScale;
    public Vector3 minScale;
    public Vector3 maxScale;
    public int wobbleCount;
    public float wobbleDuration;

    Sequence currentSequence;

    private void Awake()
    {
        baseScale = transform.localScale;
    }

    public void WobbleAnimation()
    {
        if (currentSequence != null)
            return;

        currentSequence = DOTween.Sequence();

        float dur = wobbleDuration / wobbleCount;

        for (int i = 0; i < wobbleCount - 1; i++)
        {
            Vector3 target = new Vector3(
                UnityEngine.Random.Range(minScale.x * baseScale.x, maxScale.x * baseScale.x),
                UnityEngine.Random.Range(minScale.y * baseScale.y, maxScale.y * baseScale.y),
                UnityEngine.Random.Range(minScale.z * baseScale.z, maxScale.z * baseScale.z)); ;
            currentSequence.Append(transform.DOScale(target, dur));
        }
        currentSequence.Append(transform.DOScale(baseScale, dur));

        currentSequence.OnComplete(KillSequence);
    }

    private void KillSequence()
    {
        if (currentSequence != null)
            currentSequence.Kill();
        currentSequence = null;
    }

    public Sequence Die()
    {
        KillSequence();

        currentSequence = DOTween.Sequence();

        currentSequence.Append(transform.DOScale(3, 0.3f));
        currentSequence.Join(GetComponent<MeshRenderer>().material.DOFade(0, 0.3f));

        return currentSequence;
    }
}
