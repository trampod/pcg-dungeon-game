using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    public GameObject spawnPrefab;

    private void Awake()
    {
        PlayerScript player = FindObjectOfType<PlayerScript>();
        if (player == null)
        {
            GameObject go = Instantiate(spawnPrefab,transform.position,transform.rotation);
            DontDestroyOnLoad(go);
        }
        else
        {
            CharacterController controller = player.GetComponent<CharacterController>();
            controller.enabled = false;
            player.transform.position = transform.position;
            player.transform.rotation = transform.rotation;
            controller.enabled = true;
        }
        Destroy(this);
    }
}
