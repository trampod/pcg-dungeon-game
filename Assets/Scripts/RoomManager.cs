using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    [SerializeField]
    private List<RoomTemplate> roomTemplates = new List<RoomTemplate>();

    [Separator("Room Probability")]
    public float ArmoryChance;
    public float LivingQuartersChance;

    public RoomTemplate RandomRoom()
    {
        return roomTemplates.GetRandom();
    }

    public void SetRandomRoomType(RoomTemplate room)
    {
        float rnd = Random.Range(0f,1f);

        if(rnd < ArmoryChance)
        {
            room.Type = RoomType.Armory;
            return;
        }
        rnd -= ArmoryChance;
        if(rnd < LivingQuartersChance)
        {
            room.Type = RoomType.LivingQuarters;
            return;
        }
        room.Type = RoomType.GeneralRoom;
    }

    private void OnValidate()
    {
        float sum = ArmoryChance + LivingQuartersChance;
        if(sum > 1f)
        {
            ArmoryChance /= sum;
            LivingQuartersChance /= sum;
        }
    }
}
