using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelsManager : MonoBehaviour
{
    [Separator("Room Models")]
    public GameObject FloorModel;
    public GameObject CeilingModel;
    public GameObject WallModel;
    public GameObject DoorModel;
    [Separator("Prop Models")]
    public GameObject EntranceModel;
    public GameObject ExitModel;

    private void Awake()
    {
        _instance = this;
    }

    private static ModelsManager _instance;
    public static ModelsManager Instance => _instance;
}
