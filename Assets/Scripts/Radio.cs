using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Radio : MonoBehaviour
{
    public List<AudioClip> playlist;
    private AudioSource audioSource;

    void Start()
    {

        if(playlist.Count != 0)
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = playlist.GetRandom();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(audioSource != null)
        {
            if(!audioSource.isPlaying)
            {
                audioSource.clip = playlist.GetRandom();
                audioSource.Play();
            }
        }
    }
}
