using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalAnimation : MonoBehaviour
{
    public float delay = 1;
    public float duration = 1;

    private void Awake()
    {
        Vector3 tmp = transform.localScale;
        transform.localScale = new Vector3 (0, tmp.y, tmp.z);

        Sequence seq = DOTween.Sequence();

        seq.Append(transform.DOScaleX(0, delay));

        seq.Append(transform.DOScale(tmp, duration));
    }
}
