using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridVisualisation : MonoBehaviour
{
    [SerializeField]
    private DungeonGenerator dungeon;

    public GameObject Box;
    public Material HallMaterial;
    public Material RoomMaterial;
    public Material DoorMaterial;
    public Material ReservedMaterial;
    public Material PropMaterial;
    public Material EntranceMaterial;
    public Material EscapeMaterial;

    [ContextMenu("Regenerate")]
    public void RegenerateVisualisation()
    {
        ClearVisualisation();

        for (int x = 0; x < dungeon.DungeonSize.x; x++)
        {
            for (int y = 0; y < dungeon.DungeonSize.y; y++)
            {
                for (int z = 0; z < dungeon.DungeonSize.z; z++)
                {
                    if(dungeon.DungeonGrid[x, y, z] != DungeonTile.Empty)
                    {
                        GameObject box = Instantiate(Box, new Vector3(x, y, z), transform.rotation, transform);
                        switch (dungeon.DungeonGrid[x, y, z])
                        {
                            case DungeonTile.Hall:
                                box.GetComponent<MeshRenderer>().material = HallMaterial;
                                break;
                            case DungeonTile.Room:
                                box.GetComponent<MeshRenderer>().material = RoomMaterial;
                                break;
                            case DungeonTile.Door:
                                box.GetComponent<MeshRenderer>().material = DoorMaterial;
                                break;
                            case DungeonTile.Reserved:
                                box.GetComponent<MeshRenderer>().material = ReservedMaterial;
                                break;
                            case DungeonTile.Prop:
                                box.GetComponent<MeshRenderer>().material = PropMaterial;
                                break;
                            case DungeonTile.Entrance:
                                box.GetComponent<MeshRenderer>().material = EntranceMaterial;
                                break;
                            case DungeonTile.Escape:
                                box.GetComponent<MeshRenderer>().material = EscapeMaterial;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    public void ClearVisualisation()
    {
        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
}
