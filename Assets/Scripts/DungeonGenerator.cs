using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;
using static Delaunay;

public enum DungeonTile
{
    Empty,
    Hall,
    Room,
    Door,
    Reserved,
    Prop,
    Entrance,
    Escape
}

public class DungeonGenerator : MonoBehaviour
{
    [Separator("Debug")]
    public bool HideGUILayout;

    [Separator("Generator")]

    private RoomManager roomManager;

    [SerializeField]
    public List<RoomTemplate> Rooms = new List<RoomTemplate>();

    [MinMaxRange(5, 20)]
    // Will be set to 90-100 by default in inspector
    public RangedInt RoomCount = new(8, 12);

    public int NumberOfTries;

    public Vector3Int DungeonSize = new Vector3Int(20, 5, 20);

    public GridVisualisation gridVisualisation;


    public DungeonTile[,,] DungeonGrid;

    public int Seed = -1;


    [Separator]
    [Header("Final Generated Values")]
    [ReadOnly]
    public int FinalRoomCount;

    private GameObject mapHolder;
    private GameObject modelsHolder;

    [SerializeField]
    public Pathfinder Pathfinder;

    [Separator("Enemy Spawn")]
    public GameObject EnemyPrefab;
    public float HallSpawnChance;

    void Awake()
    {
        roomManager = FindObjectOfType<RoomManager>();
        if (gridVisualisation == null)
            gridVisualisation = FindObjectOfType<GridVisualisation>();

        //Generate();
    }

    private void OnGUI()
    {
        if (HideGUILayout)
            return;

        GUILayout.Label($"Seed = {Seed}");
        if (GUILayout.Button("Reset Seed"))
        {
            Seed = -1;
            _nextStep = 0;
        }
        GUILayout.Label("");
        GUILayout.Label($"Size = {DungeonSize}");
        GUILayout.Label($"Rooms = {FinalRoomCount}");
        if (GUILayout.Button("Start New"))
        {
            Seed++;
            Random.InitState(Seed);
            ClearDungeon();
            //Generate();
            _nextStep = 0;
            NextGenerationStep();
            if (gridVisualisation != null)
                gridVisualisation.RegenerateVisualisation();
            graph = null;
        }

        if (_nextStep > 0)
        {
            if (GUILayout.Button("Next Step"))
            {
                NextGenerationStep();
                if (gridVisualisation != null)
                    gridVisualisation.RegenerateVisualisation();
            }
        }
        else if (previewGenerated)
        {
            if (GUILayout.Button("Generate Models"))
            {
                GenerateModels();
                var controller = FindObjectOfType<CameraController>();
                if (controller != null)
                    controller.transform.position = DungeonSize * 2;
                if (gridVisualisation != null)
                    gridVisualisation.gameObject.SetActive(false);
                previewGenerated = false;
            }
        }
        else
        {
            GUILayout.Label("");
        }

        if (GUILayout.Button("Generate Complete New"))
        {
            Seed++;
            Random.InitState(Seed);
            ClearDungeon();
            GeneratePreview();
            _nextStep = 0;
            if (gridVisualisation != null)
                gridVisualisation.RegenerateVisualisation();
            previewGenerated = true;
        }

        if (GUILayout.Button("Generate New Final"))
        {
            Seed++;
            Random.InitState(Seed);
            ClearDungeon();
            GeneratePreview();
            GenerateModels();
            _nextStep = 0;
            if (gridVisualisation != null)
                gridVisualisation.RegenerateVisualisation();
            previewGenerated = false;
            var controller = FindObjectOfType<CameraController>();
            if (controller != null)
                controller.transform.position = DungeonSize * 2;
            if (gridVisualisation != null)
                gridVisualisation.gameObject.SetActive(false);
        }
    }

    private bool previewGenerated = false;

    public void GeneratePreview()
    {
        GeneratorInit();
        GenerateRooms();
        GenerateGraphs();
        PruneGraph();
        ClearReserved();
        GenerateFullFloorPaths();
        GenerateRoomTypes();
        //GenerateModels();

        //gridVisualisation.RegenerateVisualisation();
    }

    public void GenerateDungeon()
    {
        GeneratePreview();
        GenerateModels();
    }

    private int _nextStep;

    [ContextMenu(nameof(NextGenerationStep))]
    public void NextGenerationStep()
    {
        switch (_nextStep)
        {
            case 0:
                GeneratorInit();
                break;
            case 1:
                GenerateRooms();
                break;
            case 2:
                GenerateGraphs();
                break;
            case 3:
                PruneGraph();
                break;
            case 4:
                ClearReserved();
                GenerateFullFloorPaths();
                break;
            case 5:
                GenerateRoomTypes();
                previewGenerated = true;
                _nextStep = -1;
                break;
            //case 6:
            //    GenerateModels();
            //    break;
            default:
                _nextStep = 0;
                return;
        }


        _nextStep++;
    }

    private void GenerateModels()
    {
        GenerateFloorModels();
        GenerateCeilingModels();
        GenerateWallModels();
        GenerateProps();
        GenerateEnemies();
    }

    private void GenerateEnemies()
    {
        Quaternion rotation = Quaternion.identity;

        for (int x = 0; x < DungeonSize.x; x++)
        {
            for (int y = 0; y < DungeonSize.y; y++)
            {
                for (int z = 0; z < DungeonSize.z; z++)
                {
                    if(DungeonGrid[x, y, z] == DungeonTile.Hall)
                    {
                        if(Random.Range(0f,1f) < HallSpawnChance)
                        {
                            if(EnemyPrefab != null)
                            {
                                Instantiate(EnemyPrefab, new Vector3(x,y,z) * 4, rotation);
                            }
                        }
                    }
                }
            }
        }
    }

    private void GenerateProps()
    {
        foreach (RoomTemplate room in Rooms)
        {
            foreach (System.Tuple<Vector3, PropType> item in room.PlacedProps)
            {
                GameObject prefab = PropManager.Instance.GetPropPrefab(item.Item2);

                Instantiate(prefab, item.Item1 * 4, Quaternion.identity, modelsHolder.transform);
            }
        }
    }

    private void GenerateWallModels()
    {
        Quaternion rotation = Quaternion.identity;

        for (int x = 0; x < DungeonSize.x; x++)
        {
            for (int y = 0; y < DungeonSize.y; y++)
            {
                for (int z = 0; z < DungeonSize.z; z++)
                {
                    DungeonTile tile = DungeonGrid[x, y, z];
                    Vector3Int pos = new Vector3Int(x, y, z);
                    if (tile != DungeonTile.Empty)
                    {
                        var neighbours = Neighbours(pos);

                        foreach (var neighbor in neighbours)
                        {
                            if (neighbor.Item2 == DungeonTile.Empty || (tile == DungeonTile.Hall && neighbor.Item2 == DungeonTile.Room))
                            {
                                GameObject obj = Instantiate(ModelsManager.Instance.WallModel, (pos + neighbor.Item1) * 2, rotation, modelsHolder.transform);
                                obj.transform.localScale = Vector3.one;
                                obj.transform.LookAt(pos * 4);
                            }
                            else if (neighbor.Item2 == DungeonTile.Door && tile == DungeonTile.Hall)
                            {
                                GameObject obj = Instantiate(ModelsManager.Instance.DoorModel, (pos + neighbor.Item1) * 2, rotation, modelsHolder.transform);
                                obj.transform.localScale = Vector3.one;
                                obj.transform.LookAt(pos * 4);
                            }
                        }
                    }
                }
            }
        }
    }

    private List<System.Tuple<Vector3Int, DungeonTile>> PartialNeighbours(Vector3Int position)
    {
        List<System.Tuple<Vector3Int, DungeonTile>> tuples = new List<System.Tuple<Vector3Int, DungeonTile>>();

        List<Vector3Int> positions = new List<Vector3Int>();
        List<Vector3Int> fakePos = new List<Vector3Int>();
        if (position.x > 0)
            positions.Add(position + Vector3Int.left);
        else
            fakePos.Add(position + Vector3Int.left);

        if (position.z > 0)
            positions.Add(position + Vector3Int.back);
        else
            fakePos.Add(position + Vector3Int.left);


        foreach (Vector3Int pos in positions)
        {
            tuples.Add(new System.Tuple<Vector3Int, DungeonTile>(pos, DungeonGrid[pos.x, pos.y, pos.z]));
        }
        foreach (Vector3Int pos in fakePos)
        {
            tuples.Add(new System.Tuple<Vector3Int, DungeonTile>(pos, DungeonTile.Empty));
        }

        return tuples;
    }


    private List<System.Tuple<Vector3Int, DungeonTile>> Neighbours(Vector3Int position)
    {
        List<System.Tuple<Vector3Int, DungeonTile>> tuples = new List<System.Tuple<Vector3Int, DungeonTile>>();

        List<Vector3Int> positions = new List<Vector3Int>();
        List<Vector3Int> fakePos = new List<Vector3Int>();
        if (position.x > 0)
            positions.Add(position + Vector3Int.left);
        else
            fakePos.Add(position + Vector3Int.left);

        if (position.x < DungeonSize.x - 1)
            positions.Add(position + Vector3Int.right);
        else
            fakePos.Add(position + Vector3Int.left);

        if (position.z > 0)
            positions.Add(position + Vector3Int.back);
        else
            fakePos.Add(position + Vector3Int.left);

        if (position.z < DungeonSize.z - 1)
            positions.Add(position + Vector3Int.forward);
        else
            fakePos.Add(position + Vector3Int.left);


        foreach (Vector3Int pos in positions)
        {
            tuples.Add(new System.Tuple<Vector3Int, DungeonTile>(pos, DungeonGrid[pos.x, pos.y, pos.z]));
        }
        foreach (Vector3Int pos in fakePos)
        {
            tuples.Add(new System.Tuple<Vector3Int, DungeonTile>(pos, DungeonTile.Empty));
        }

        return tuples;
    }

    private void GenerateCeilingModels()
    {
        Quaternion rotation = Quaternion.Euler(180f, 0f, 0f);

        for (int x = 0; x < DungeonSize.x; x++)
        {
            for (int y = 0; y < DungeonSize.y; y++)
            {
                for (int z = 0; z < DungeonSize.z; z++)
                {
                    DungeonTile tile = DungeonGrid[x, y, z];
                    Vector3 pos = new Vector3(x, y, z);
                    if (tile != DungeonTile.Empty)
                    {
                        GameObject obj = Instantiate(ModelsManager.Instance.CeilingModel, pos * 4 + Vector3.up * 4, rotation, modelsHolder.transform);
                        obj.transform.localScale = Vector3.one;
                    }
                }
            }
        }
    }

    private void GenerateFloorModels()
    {
        Quaternion rotation = Quaternion.identity;

        for (int x = 0; x < DungeonSize.x; x++)
        {
            for (int y = 0; y < DungeonSize.y; y++)
            {
                for (int z = 0; z < DungeonSize.z; z++)
                {
                    DungeonTile tile = DungeonGrid[x, y, z];
                    Vector3 pos = new Vector3(x, y, z);
                    if (tile != DungeonTile.Empty)
                    {
                        GameObject obj = Instantiate(ModelsManager.Instance.FloorModel, pos * 4, rotation, modelsHolder.transform);
                        obj.transform.localScale = Vector3.one;
                    }
                }
            }
        }
    }

    private void GenerateRoomTypes()
    {
        foreach (RoomTemplate room in Rooms)
        {
            roomManager.SetRandomRoomType(room);
            room.PlaceProps();
        }

        List<RoomTemplate> access = Rooms.GetRandom(2);
        access[0].SetAccess(AccessType.Entrance);
        access[1].SetAccess(AccessType.Escape);
    }

    private void PruneGraph()
    {
        List<Edge> skeleton;
        List<Edge> rermaining;

        Graph.Skeleton(graph, out skeleton, out rermaining);

        graph = skeleton;

        foreach (Edge e in rermaining)
        {
            if (Random.Range(0f, 1f) < edgeSaveChance)
            {
                graph.Add(e);
            }
        }
    }

    private void GeneratorInit()
    {
        mapHolder = new GameObject("Map Holder");
        mapHolder.transform.parent = transform;
        modelsHolder = new GameObject("Models Holder");
        modelsHolder.transform.parent = transform;
        DungeonGrid = new DungeonTile[DungeonSize.x, DungeonSize.y, DungeonSize.z];
        FinalRoomCount = Random.Range(RoomCount.Min, RoomCount.Max + 1);
        previewGenerated = false;
        var controller = FindObjectOfType<CameraController>();
        if (controller != null)
            controller.transform.position = DungeonSize / 2;
        if (gridVisualisation != null)
            gridVisualisation.gameObject.SetActive(true);
    }

    private void ClearReserved()
    {
        for (int x = 0; x < DungeonSize.x; x++)
        {
            for (int y = 0; y < DungeonSize.y; y++)
            {
                for (int z = 0; z < DungeonSize.z; z++)
                {
                    if (DungeonGrid[x, y, z] == DungeonTile.Reserved)
                        DungeonGrid[x, y, z] = DungeonTile.Empty;
                }
            }
        }
    }

    private void GenerateRooms()
    {
        for (int i = 0; i < FinalRoomCount; i++)
        {
            for (int j = 0; j < NumberOfTries; j++)
            {
                if (PlaceRoom())
                    break;
            }
        }
    }

    private bool PlaceRoom()
    {
        RoomTemplate room = roomManager.RandomRoom();

        int rotation = Random.Range(0, 4);
        bool willRotate = rotation % 2 > 0;
        bool mirrorX = Random.Range(0, 2) > 0;
        bool mirrorZ = Random.Range(0, 2) > 0;

        Vector3Int position = new Vector3Int(
            Random.Range(1, DungeonSize.x - (willRotate ? room.Size.z : room.Size.x) - 1),
            Random.Range(0, DungeonSize.y - room.Size.y),
            Random.Range(1, DungeonSize.z - (willRotate ? room.Size.x : room.Size.z) - 1));

        if (!CanPlace(room, position, willRotate))
            return false;


        RoomTemplate go = Instantiate(room, position, transform.rotation, mapHolder.transform);
        go.Rotated = willRotate;

        go.Rotation = rotation;
        go.MirrorX = mirrorX;
        go.MirrorZ = mirrorZ;
        go.Position = position;

        go.SetTiles(ref DungeonGrid);

        Rooms.Add(go);
        return true;
    }

    private bool CanPlace(RoomTemplate room, Vector3Int position, bool rotate)
    {
        //Debug.Log("CanPlace()");
        //Debug.Log($"room.Size={room.Size}");
        //Debug.Log($"position={position}");
        //Debug.Log($"rotate={rotate}");
        int SizeX = rotate ? room.Size.z : room.Size.x;
        int SizeZ = rotate ? room.Size.x : room.Size.z;
        for (int x = 0; x < SizeX; x++)
        {
            for (int y = 0; y < room.Size.y; y++)
            {
                for (int z = 0; z < SizeZ; z++)
                {
                    //Debug.Log($"xyz={x},{y},{z}");
                    if (DungeonGrid[position.x + x, position.y + y, position.z + z] != DungeonTile.Empty)
                        return false;
                }
            }
        }
        return true;
    }

    private List<Edge> graph;
    private List<Vertex> allVectors;
    public float edgeSaveChance = 0.5f;

    private void GenerateGraphs()
    {
        List<RoomTemplate>[] floors = GetFloorRooms();
        graph = new List<Edge>();
        allVectors = new List<Vertex>();

        foreach (List<RoomTemplate> rooms in floors)
        {
            List<Vertex> vectors = new List<Vertex>();

            foreach (RoomTemplate room in rooms)
            {
                vectors.Add(new Vertex<RoomTemplate>(room.middlePoint, room));
            }

            //Debug.Log(vectors.Count);
            if (vectors.Count == 2)
                graph.Add(new Edge(vectors[0], vectors[1]));
            if (vectors.Count == 3)
            {
                graph.Add(new Edge(vectors[0], vectors[1]));
                graph.Add(new Edge(vectors[1], vectors[2]));
                graph.Add(new Edge(vectors[2], vectors[0]));
            }
            else if (vectors.Count > 3)
            {
                Delaunay delaunay = Delaunay.Triangulate(vectors);
                //Debug.Log($"delunay.edges.count={delaunay.Edges.Count}");
                graph.AddRange(delaunay.Edges);
            }

            //allVectors.AddRange(vectors);
        }
    }

    private List<RoomTemplate>[] GetFloorRooms()
    {
        List<RoomTemplate>[] floors = new List<RoomTemplate>[DungeonSize.y];

        for (int i = 0; i < DungeonSize.y; i++)
        {
            floors[i] = new List<RoomTemplate>();
        }

        foreach (RoomTemplate room in Rooms)
        {
            List<int> doorFloors = new List<int>();

            // I only want to make the graph for floors the room has doors on... left from time there were suposed to be tall rooms... which for now there won't... yet
            List<Vector3Int> doorPositions = room.GetDoorTiles();
            foreach (Vector3Int door in doorPositions)
            {
                if (!doorFloors.Contains(door.y))
                {
                    doorFloors.Add(door.y);
                }
            }

            foreach (int floor in doorFloors)
            {
                //Debug.Log($"floor={floor}");
                floors[floor].Add(room);
            }
        }

        return floors;
    }

    private void GenerateFullFloorPaths()
    {

        foreach (Edge edge in graph)
        {
            //Debug.Log($"edge.V={edge.V.GetType()}, edge.U={edge.U.GetType()}");
            Vertex<RoomTemplate> v = edge.V as Vertex<RoomTemplate>;
            Vertex<RoomTemplate> u = edge.U as Vertex<RoomTemplate>;
            //Debug.Log($"edge.V.item={v.Item}, edge.U={u.Item}");

            List<Vector3Int> path = Pathfinder.CreatePath(u.Item, v.Item);

            foreach (Vector3Int p in path)
            {
                if (DungeonGrid[p.x, p.y, p.z] == DungeonTile.Empty || DungeonGrid[p.x, p.y, p.z] == DungeonTile.Reserved)
                    DungeonGrid[p.x, p.y, p.z] = DungeonTile.Hall;
            }
        }
    }

    public void ClearDungeon()
    {
        if (mapHolder != null)
        {
            Destroy(mapHolder);
            mapHolder = null;
        }
        if (modelsHolder != null)
        {
            Destroy(modelsHolder);
            modelsHolder = null;
        }
        Rooms.Clear();
    }

    private void OnDrawGizmos()
    {
        if (gridVisualisation == null)
            return;

        //Debug.Log(graph == null);
        if (graph != null)
        {
            //Debug.Log(graph.Count);
            Gizmos.color = Color.yellow;
            foreach (Edge edge in graph)
            {
                //Debug.Log(edge.U + "x" + edge.V);
                Gizmos.DrawLine(edge.U.Position, edge.V.Position);
            }
        }

        if (allVectors != null)
        {

            Gizmos.color = Color.cyan;
            foreach (Vertex vector in allVectors)
            {
                Gizmos.DrawSphere(vector.Position, 1);
            }
        }

        foreach(RoomTemplate room in Rooms)
        {

            foreach (System.Tuple<Vector3, PropType> prop in room.PlacedProps)
            {
                switch (prop.Item2)
                {
                    case PropType.Escape:
                        Gizmos.color = Color.black;
                        break;
                    case PropType.Entrance:
                        Gizmos.color = Color.white;
                        break;
                    case PropType.GeneralRoom:
                        Gizmos.color = Color.red;
                        break;
                    case PropType.Armory:
                        Gizmos.color = Color.cyan;
                        break;
                    case PropType.LivingQuarters:
                        Gizmos.color = Color.yellow;
                        break;
                    default:
                        break;
                }
                Gizmos.DrawSphere(prop.Item1 + Vector3.up, 0.5f);
            }
        }
    }
}
