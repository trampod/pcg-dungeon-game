using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAfter(typeof(DungeonGenerator))]
public class GameMaster : MonoBehaviour
{
    private GameMaster _instance;
    public GameMaster Instance
    {
        get
        {
            if (_instance == null)
            {
                if (_instance != null) return _instance;
                _instance = FindObjectOfType<GameMaster>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = nameof(GameMaster);
                    _instance = go.AddComponent<GameMaster>();
                }
                GameObject.DontDestroyOnLoad(_instance);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        _instance = this;
        GameObject.DontDestroyOnLoad(_instance);

        DungeonGenerator generator = FindObjectOfType<DungeonGenerator>();
        System.Random random = new System.Random();
        generator.Seed = random.Next(int.MaxValue);

        generator.GenerateDungeon();
    }
}
