using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    public float Timer = 10f;

    void Start()
    {
        Invoke(nameof(Suicide), Timer);
    }

    private void Suicide()
    {
        Destroy(gameObject);
    }
}
