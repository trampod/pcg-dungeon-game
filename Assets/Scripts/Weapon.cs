using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct AnimState
{
    public Vector3 Position;
    public Vector3 Rotation;
    public Ease EaseFunction;
}

public class Weapon : MonoBehaviour
{
    public float Speed;
    public float Return;
    public float Damage;
    public float KnockBackStrength;
    public float BlockReductionMod;
    public float BlockReductionFlat;

    public AnimState BaseState;
    public AnimState AttackState;
    public AnimState BlockState;

    private Sequence _sequence;

    private bool isAttacking = false;
    private HashSet<GameObject> _hitObjects;

    AudioSource sfx;


    public int Kills = 0;

    private void Awake()
    {
        _hitObjects = new HashSet<GameObject>();
        sfx = GetComponent<AudioSource>();
    }

    public void AttackMotion()
    {
        //KillTween();
        if (_sequence != null)
            return;

        isAttacking = true;

        _sequence = DOTween.Sequence();

        _sequence.Append(transform.DOLocalMove(AttackState.Position, Speed).SetEase(AttackState.EaseFunction));
        _sequence.Join(transform.DOLocalRotate(AttackState.Rotation, Speed).SetEase(AttackState.EaseFunction));
        _sequence.Append(transform.DOLocalMove(BaseState.Position, Return).SetEase(AttackState.EaseFunction));
        _sequence.Join(transform.DOLocalRotate(BaseState.Rotation, Return).SetEase(AttackState.EaseFunction));

        _sequence.OnComplete(StopAttack);
    }

    private void KillTween()
    {
        if(_sequence != null)
        {
            _sequence.Kill();
            _sequence = null;
        }
    }

    private void StopAttack()
    {
        KillTween();
        isAttacking = false;
        _hitObjects.Clear();
    }

    private void OnTriggerStay(Collider other)
    {
        if(isAttacking)
        {
            SlimeScript slime = other.gameObject.GetComponent<SlimeScript>();
            if(slime != null && !_hitObjects.Contains(slime.gameObject))
            {
                if(_hitObjects.Count == 0 && sfx != null)
                    sfx.Play();

                //isAttacking = false;
                _hitObjects.Add(slime.gameObject);
                if (slime.DealDamage(Damage))
                    Kills++;
                Rigidbody rb = slime.GetComponent<Rigidbody>();

                if(rb != null)
                {
                    Vector3 vector = slime.transform.position - transform.position;
                    vector.Normalize();
                    vector += Vector3.up;
                    rb.AddForce(vector * KnockBackStrength);
                }

                //slime.CurrentHealth -= Damage;
            }
        }
    }
}
