using DG.Tweening;
using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class SlimeScript : MonoBehaviour
{
    public SlimeAnimator Body;
    public SlimeAnimator Core;

    public GameObject Blob;

    private Rigidbody _rigidbody;
    private Collider _collider;


    [Separator("AI")]
    public float ActionDelay = 0.3f;
    public float VisionRange = 10f;
    public float AttackRange = 4f;
    public PlayerScript target = null;

    [Separator("Stats")]
    [Header("Main Attributes")]
    [Range(0f, 1f)] public float Speed;
    [Range(0f, 1f)] public float Strength;
    [Range(0f, 1f)] public float Resistance;
    [Header("Derived Attributes")]
    [ReadOnly] public float JumpForce;
    [ReadOnly] public float Damage;
    [ReadOnly] public float MaxHealth;
    [ReadOnly] public float CurrentHealth;
    [Separator("Stats Settings")]
    [Header("Speed Settings")]
    public float MinSpeed = 5;
    public float MaxSpeed = 20;
    public Color SpeedColor = Color.blue;
    [Header("Strength Settings")]
    public float MinStrength = 5;
    public float MaxStrength = 20;
    public Color StrengthColor = Color.red;
    [Header("Resistance")]
    public float MinResistance = 5;
    public float MaxResistance = 20;
    public Color ResistanceColor = Color.green;

    [Header("HSV color")]
    public Gradient gradient;
    public float MinSaturation;
    public float MaxSaturation;
    public float MinLight;
    public float MaxLight;

    private float _counter;

    public bool IsInAir { get; private set; }

    private bool isAttacking = false;


    private Color finalColor;
    private AudioSource sfx;

    void OnValidate()
    {
        RecalculateAttributes();
    }

    private void RecalculateAttributes()
    {
        JumpForce = Mathf.Lerp(MinSpeed, MaxSpeed, Speed);
        Damage = Mathf.Lerp(MinStrength, MaxStrength, Strength);
        MaxHealth = Mathf.Lerp(MinResistance, MaxResistance, Resistance);
        CurrentHealth = MaxHealth;
    }

    public bool DealDamage(float damage)
    {
        if(sfx != null)
            sfx.Play();
        //Debug.Log($"Slime.DealDamage(): {CurrentHealth}/{MaxHealth} - {damage} ->");
        CurrentHealth -= damage;
        //Debug.Log($"Slime.DealDamage(): {CurrentHealth}/{MaxHealth}");
        if (CurrentHealth <= 0)
        {
            Die();
            return true;
        }
        return false;
    }

    private void Die()
    {
        //Debug.Log($"Slime.Die(): {CurrentHealth}/{MaxHealth}");
        Body.Die().OnComplete(()=>Destroy(this.gameObject));
    }

    private void SetColor()
    {
        if (Body == null)
            return;

        MeshRenderer rendererBody = Body.GetComponent<MeshRenderer>();
        MeshRenderer rendererCore = Core.GetComponent<MeshRenderer>();

        if (rendererBody == null)
            return;

        Color color = Color.green;

        Vector3 tmp = new Vector3(
            SpeedColor.r * Speed + StrengthColor.r * Strength + ResistanceColor.r * Resistance,
            SpeedColor.g * Speed + StrengthColor.g * Strength + ResistanceColor.g * Resistance,
            SpeedColor.b * Speed + StrengthColor.b * Strength + ResistanceColor.b * Resistance);

        tmp /= 3;

        transform.localScale = Vector3.one * Mathf.Lerp(1f, 1.4f, Resistance);

        color = new Color(tmp.x, tmp.y, tmp.z, rendererBody.material.color.a);


        //Color gradColor = gradient.Evaluate(Resistance);

        //Color.RGBToHSV(gradColor, out float Hue, out _, out _);

        //color = Color.HSVToRGB(Hue, Mathf.Lerp(MinSaturation, MaxSaturation, Strength), Mathf.Lerp(MinLight, MaxLight, Speed));
        //color.a = renderer.material.color.a;

        rendererBody.material.color = color;
        rendererCore.material.color = color;
        finalColor = color;
    }

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        if(_rigidbody == null)
            Destroy(gameObject);

        _collider = GetComponent<Collider>();
        if (_collider == null)
            Destroy(gameObject);

        target = FindObjectOfType<PlayerScript>();

        sfx = GetComponent<AudioSource>();

        RandomizeStats();
        SetColor();
    }

    private void RandomizeStats()
    {
        Vector3 tmp = new Vector3(
            UnityEngine.Random.Range(0f, 1f),
            UnityEngine.Random.Range(0f, 1f),
            UnityEngine.Random.Range(0f, 1f));

        int rnd = UnityEngine.Random.Range(0, 3);

        switch(rnd)
        {
            case 0:
                tmp.x = 0;
                break;
            case 1:
                tmp.y = 0;
                break;
            case 2:
                tmp.z = 0;
                break;
            default:
                break;
        }

        tmp.Normalize();

        Speed = tmp.x;
        Strength = tmp.y;
        Resistance = tmp.z;

        RecalculateAttributes();
    }

    void Update()
    {
        if(CurrentHealth > 0 && IsGrounded())
        {
            _counter -= Time.deltaTime;

            if(_counter < 0)
            {
                if(CanSeePlayer())
                {
                    Vector3 direction = (target.transform.position + Vector3.up * 2) - this.transform.position;
                    if(direction.magnitude < AttackRange)
                    {
                        direction.Normalize();
                        Jump(direction,2+Strength);
                        isAttacking = true;
                    }
                    else
                    {
                        direction.y = 0;
                        direction.Normalize();
                        direction += Vector3.up;
                        direction.Normalize();
                        Jump(direction);
                    }
                }
                else
                {
                    RandomJump();
                }
                
                _counter = ActionDelay;
            }
        }
    }

    private bool IsGrounded()
    {
        LayerMask mask = LayerMask.GetMask("Slime");
        
        return Physics.CheckCapsule(_collider.bounds.center,
            new Vector3(
                _collider.bounds.center.x,
                _collider.bounds.min.y,// - 0.1f,
                _collider.bounds.center.z),
            0.05f, ~mask);
    }

    private void OnDrawGizmos()
    {
        if(_collider)
        {

            if (IsGrounded())
                Gizmos.color = Color.white;
            else
                Gizmos.color = Color.yellow;

            Gizmos.DrawSphere(_collider.bounds.center, 0.1f);
            Gizmos.DrawSphere(new Vector3(
                    _collider.bounds.center.x,
                    _collider.bounds.min.y,
                    _collider.bounds.center.z), 0.05f);
        }
    }

    private void RandomJump()
    {
        Vector3 jumpVector = Vector3.up;

        Vector3 direction = Quaternion.Euler(new Vector3(0, UnityEngine.Random.Range(0f,360f),0)) * Vector3.forward;


        jumpVector += direction;

        Jump(jumpVector);
    }

    private void Jump(Vector3 direction, float forceModifier = 1)
    {
        direction.Normalize();

        _rigidbody.AddForce(direction * JumpForce * forceModifier * UnityEngine.Random.Range(0.9f, 1.1f));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (CurrentHealth <= 0)
            return;

        //BodyWobbleAnimation();
        Body.WobbleAnimation();
        //Core.WobbleAnimation();

        _counter = ActionDelay;
        if(sfx != null)
            sfx.Play();


        PlayerScript player = collision.gameObject.GetComponent<PlayerScript>();
        
        
        if(isAttacking)
        {
            isAttacking = false;
            if(player != null)
            {
                //player.Health -= Damage;
                player.DealDamage(Damage);
            }
        }
        

        SlimeScript slime = collision.gameObject.GetComponent<SlimeScript>();
        if(player == null && slime == null && Blob != null)
        {
            GameObject go = Instantiate(Blob,collision.contacts[0].point,Quaternion.identity);
            go.transform.LookAt(transform.position);
            SelfDestruct sd = go.GetComponent<SelfDestruct>();
            MeshRenderer[] mrs = go.GetComponentsInChildren<MeshRenderer>();
            foreach(MeshRenderer mr in mrs)
            {
                mr.material.color = finalColor;
                mr.material.DOFade(0f, sd.Timer).SetEase(Ease.OutQuad);
            }
        }
    }

    public bool CanSeePlayer()
    {
        if (target == null)
            return false;

        //Debug.Log($"this={transform.position} target={target.transform.position}");

        Ray ray = new Ray(transform.position, (target.transform.position + Vector3.up) - transform.position);
        RaycastHit hit;

        //Debug.Log($"origin={ray.origin} target={ray.direction}");
        Debug.DrawRay(ray.origin, ray.direction, Color.magenta);
        Debug.DrawLine(ray.origin, ray.direction, Color.yellow);

        LayerMask mask = LayerMask.GetMask("Default", "Player");

        if (Physics.Raycast(ray, out hit, VisionRange, mask, QueryTriggerInteraction.Ignore))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.magenta);

            if (hit.collider.GetComponent<PlayerScript>() == null)
                return false;

            return hit.distance < VisionRange;
        }


        return false;
    }
}
