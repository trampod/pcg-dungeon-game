using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour
{
    [Scene]
    public string Scene;

    public void SwitchScene()
    {
        SceneManager.LoadScene(Scene);
    }
}
