using MyBox;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoomType
{
    GeneralRoom, // tables, cabinets
    Armory, // weapon stands
    LivingQuarters // beds
}

public enum PropType
{
    Entrance, // hole in the ceiling
    Escape, // hole in the floor
    GeneralRoom, // tables, cabinets
    Armory, // weapon stands
    LivingQuarters // beds
}

public enum AccessType
{
    Normal,
    Entrance, // hole in the ceiling
    Escape // hole in the floor
}

public class RoomTemplate : MonoBehaviour
{
    public Vector3Int Size;


    public int Rotation = 0;
    public bool MirrorX = false;
    public bool MirrorZ = false;
    public bool Rotated = false;

    [SerializeField]
    public List<Vector3Int> DoorPositions;
    [SerializeField]
    public List<Vector3Int> PropPositions;

    public Vector3Int Position;

    public Vector3 middlePoint => new Vector3(
        transform.position.x + ((float)(Rotated ? Size.z : Size.x) - 1) / 2,
        transform.position.y + ((float)Size.y - 1) / 2,
        transform.position.z + ((float)(Rotated ? Size.x : Size.z) - 1) / 2);

    public RoomType Type = RoomType.GeneralRoom;
    public AccessType AccessType = AccessType.Normal;

    [SerializeField, ReadOnly]
    public List<Tuple<Vector3,PropType>> PlacedProps = new List<Tuple<Vector3,PropType>>();

    public DungeonTile GetTile(Vector3Int pos)
    {
        foreach (var tile in DoorPositions)
        {
            if (tile == pos)
                return DungeonTile.Door;
        }
        return DungeonTile.Room;
    }

    public List<Vector3Int> GetDoorTiles()
    {
        List<Vector3Int> result = new List<Vector3Int>();
        foreach (var tile in DoorPositions)
        {
            result.Add(GetFinalPosition(tile) + Position);
        }
        return result;
    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.white;
        //Gizmos.DrawSphere(transform.position, 1f);
        //Gizmos.color = Color.red;
        //Gizmos.DrawSphere(middlePoint, 1f);
    }

    public void SetTiles(ref DungeonTile[,,] dungeonGrid)
    {
        for (int x = 0; x < Size.x; x++)
        {
            for (int y = 0; y < Size.y; y++)
            {
                for (int z = 0; z < Size.z; z++)
                {
                    Vector3Int pos, tilePos = new Vector3Int(x, y, z);

                    pos = GetFinalPosition(tilePos);

                    dungeonGrid[Position.x + pos.x, Position.y + pos.y, Position.z + pos.z] = GetTile(tilePos);
                }
            }
        }

        for (int y = 0; y < Size.y; y++)
        {
            for (int x = -1; x <= Size.x; x++)
            {
                Vector3Int pos1, tilePos1 = new Vector3Int(x, y, Size.z);
                Vector3Int pos2, tilePos2 = new Vector3Int(x, y, -1);

                pos1 = GetFinalPosition(tilePos1);
                pos2 = GetFinalPosition(tilePos2);

                dungeonGrid[Position.x + pos1.x, Position.y + pos1.y, Position.z + pos1.z] = DungeonTile.Reserved;
                dungeonGrid[Position.x + pos2.x, Position.y + pos2.y, Position.z + pos2.z] = DungeonTile.Reserved;
            }

            for (int z = -1; z <= Size.z; z++)
            {
                Vector3Int pos1, tilePos1 = new Vector3Int(-1, y, z);
                Vector3Int pos2, tilePos2 = new Vector3Int(Size.x, y, z);

                pos1 = GetFinalPosition(tilePos1);
                pos2 = GetFinalPosition(tilePos2);

                dungeonGrid[Position.x + pos1.x, Position.y + pos1.y, Position.z + pos1.z] = DungeonTile.Reserved;
                dungeonGrid[Position.x + pos2.x, Position.y + pos2.y, Position.z + pos2.z] = DungeonTile.Reserved;
            }
        }
    }

    internal void SetAccess(AccessType access)
    {
        AccessType = access;

        int index = UnityEngine.Random.Range(0, PlacedProps.Count);
        PlacedProps[index] = new Tuple<Vector3, PropType>(PlacedProps[index].Item1, PropManager.GetPropType(access));
    }

    public void PlaceProps()
    {
        foreach(Vector3Int propPos in PropPositions)
        {
            Vector3Int pos;

            pos = GetFinalPosition(propPos);

            Vector3 placement = new Vector3(Position.x + pos.x, Position.y + pos.y, Position.z + pos.z);

            PlacedProps.Add(new Tuple<Vector3, PropType>(placement, PropManager.GetPropType(Type)));
        }
    }

    private Vector3Int GetFinalPosition(int x, int y, int z)
    {
        Vector3Int pos;
        switch (Rotation)
        {
            case 3:
                pos = new Vector3Int(z, y, Size.x - x - 1);
                break;
            case 2:
                pos = new Vector3Int(Size.x - x - 1, y, Size.z - z - 1);
                break;
            case 1:
                pos = new Vector3Int(Size.z - z - 1, y, x);
                break;
            default:
                pos = new Vector3Int(x, y, z);
                break;
        }
        pos = new Vector3Int(
            MirrorX ? (Rotation % 2 == 0 ? Size.x : Size.z) - 1 - pos.x : pos.x,
            pos.y,
            MirrorZ ? (Rotation % 2 == 0 ? Size.z : Size.x) - 1 - pos.z : pos.z);

        return pos;
    }

    private Vector3Int GetFinalPosition(Vector3Int position)
    {
        return GetFinalPosition(position.x, position.y, position.z);
    }
}
