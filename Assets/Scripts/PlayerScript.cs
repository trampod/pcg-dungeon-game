using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CharacterController))]
public class PlayerScript : MonoBehaviour
{
    Vector3 moveDirection = Vector3.zero;
    CharacterController characterController;
    Vector2 lookDirection = Vector2.zero;

    public Weapon Hand;

    public Weapon ActiveWeapon;

    public GameObject Head;

    [Header("Stats")]

    public float Speed;
    public float Health;
    public float currentHealth;
    public Vector2 LookSpeed;

    public TextMeshPro HealthText;
    public TextMeshPro KillText;

    private AudioSource sfx;

    public int MaxKills = 0;
    public int CurrentKills = 0;
    public int MaxFloor = 0;
    public int Floor = 0;

    private void Awake()
    {
        currentHealth = Health;
        characterController = GetComponent<CharacterController>();
        if(ActiveWeapon == null)
            ActiveWeapon = Hand;

        sfx = GetComponent<AudioSource>();

        UpdateHealthText();
    }

    private void UpdateHealthText()
    {
        if(HealthText != null)
        {
            HealthText.text = (int)currentHealth + "/" + Health;
        }
    }

    void Update()
    {
        CurrentKills = ActiveWeapon.Kills;
        if(CurrentKills > MaxKills)
            MaxKills = CurrentKills;
        KillText.text = $"Kills: {CurrentKills} (Max: {MaxKills})";


        if (lookDirection.y != 0)
        {
            float f = -LookSpeed.y * lookDirection.y * Time.deltaTime;
            float r = Head.transform.rotation.eulerAngles.x;
            float g = f + r;
            //Debug.Log($"Look: f:{f}, r:{r}, g:{g}");



            g = Mathf.Clamp((g + 180) % 360, 90, 270) - 180;
            if (g != f + r)
                Debug.Log($"Look: {f + r}->{g}");
            

            Head.transform.localRotation = Quaternion.Euler(g,0,0);
            //Head.transform.Rotate(new Vector3(f, 0, 0));
        }

        if(lookDirection.x != 0)
        {
            transform.Rotate(new Vector3(0, LookSpeed.x * lookDirection.x, 0)*Time.deltaTime);
        }

        if (characterController != null && moveDirection.magnitude != 0)
        {
            //moveDirection = transform.TransformDirection(Vector3.forward) * moveSpeed;
            Vector3 moveDir = transform.rotation * moveDirection;
            characterController.Move(moveDir * Speed * Time.deltaTime);
        }

        if(characterController)
        {
            characterController.Move(Vector3.down * transform.position.y);
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        Vector2 moveVal = context.ReadValue<Vector2>();

        moveVal.Normalize();

        moveDirection = new Vector3(moveVal.x, 0, moveVal.y);
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        Vector2 lookVal = context.ReadValue<Vector2>();

        lookDirection = lookVal;

        //Debug.Log($"Look={lookVal}");
    }

    public void OnAttack(InputAction.CallbackContext context)
    {
        if(ActiveWeapon != null && context.performed)
        {
            ActiveWeapon.AttackMotion();
        }
    }

    public void OnBlock(InputAction.CallbackContext context)
    {
        Debug.Log("Block");
    }

    public void OnThrow(InputAction.CallbackContext context)
    {
        Debug.Log("Throw");
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        Debug.Log("Interact");
    }

    public void DealDamage(float damage)
    {
        if (sfx != null)
            sfx.Play();

        currentHealth -= damage;
        if(currentHealth <= 0)
        {
            CurrentKills = 0;
            ActiveWeapon.Kills = 0;
            currentHealth = Health;
            UpdateHealthText();
            SceneManager.LoadScene(0);
        }
        UpdateHealthText();
    }
}
