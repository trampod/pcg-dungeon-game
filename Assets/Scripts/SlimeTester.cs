using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeTester : MonoBehaviour
{
    public List<Rigidbody> rigidbodies = new List<Rigidbody>();

    public float JumpStrength;

    public void JumpAll()
    {
        foreach(Rigidbody rigidbody in rigidbodies)
        {
            rigidbody.AddForce(Vector3.up * JumpStrength);
        }
    }
}
