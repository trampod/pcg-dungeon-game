using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct PropList
{
    public PropType PropType;
    public List<GameObject> PropPrefabs;
}

public class PropManager : MonoBehaviour
{
    private static PropManager _instance;
    public static PropManager Instance => _instance;

    private void Awake()
    {
        if( _instance == null )
            _instance = this;
    }

    [SerializeField]
    public List<PropList> PropLists = new List<PropList>();
    

    public GameObject GetPropPrefab(PropType propType)
    {
        foreach(PropList list in PropLists)
        {
            if(propType == list.PropType)
            {
                return list.PropPrefabs.GetRandom();
            }
        }
        return null;
    }



    internal static PropType GetPropType(RoomType type)
    {
        switch(type)
        {
            case RoomType.Armory:
                return PropType.Armory;
            case RoomType.LivingQuarters:
                return PropType.LivingQuarters;
            case RoomType.GeneralRoom:
            default:
                return PropType.GeneralRoom;
        }
    }

    internal static PropType GetPropType(AccessType access)
    {
        switch(access)
        {
            case AccessType.Entrance:
                return PropType.Entrance;
            case AccessType.Escape:
                return PropType.Escape;
            case AccessType.Normal:
            default:
                return PropType.GeneralRoom;
        }
    }
}
