# Proceduraly Generated Dungeons

This project is about proceduraly generated dungeons and simple game using them.

## Dungeon Generator

For the dungeon I used similar aproach to this [video]( https://www.youtube.com/watch?v=rBY2Dzej03A).

Unfortunately I had some trouble while implementing it and also due to time constraints I ended up opting to generate only a single floor. But what I have now doesn't inherently prohibits me from extending it in the future. The only parts that would need to change is initial graf generation and modifying pathfinder to allow placement of stairs.

Dungeon Generation showcase can be viewed in **DungeonGenerator** scene.


### Rooms

First step is placement of rooms. Rooms have predefined size and I added also predefined door location and props location (on the topic of props later). When placing their rotation is randomized (py steps of 90°).

Another thing is that a border is placed around them. This is done because of two things, so the rooms can be separated from each other when placing models (the dungeon is represented using tiles so any two room siles next to each other is concidered one room) and also so there is always room for halls between them.

<img src="Images/Dungeon_01.PNG" alt="Placed rooms in the world." width="300"/>

### Graph

A graph is generated to create connections between rooms that don't cross each other. This is done using Delaunay algorithm that was borrowed from the original video.

<img src="Images/Dungeon_02.PNG" alt="Generate Graph." width="300"/>

From this graph we get it's minimal spanning tree. This makes sure that each room is accessible.

<img src="Images/Dungeon_03.PNG" alt="Get minimal spanning tree." width="300"/>

And at random return some of the connections we removed earlier, so there are some possible loops in the dungeon.

<img src="Images/Dungeon_04.PNG" alt="Return random removed edges." width="300"/>

### Halls

For each edge we create path between those two rooms. This path originates and ends in the location of doors. This was done so there could be more control over the design of rooms.

Pathfinding also preferes to go thrue already existing halls so they are not overlly saturated.

<img src="Images/Dungeon_05.PNG" alt="Using pathfinding place halls." width="300"/>

### Props

Props are objects placed in the rooms so they wouldn't be empty. Each room has predefined the location of props. For each room a room type is randomly selected. Currently there are three types: Armory(cyan), Living Quarters (yellow), General room(red).

In this step the entrance (white) and exit (black) are also placed. Both are placed in separate rooms and replace on of the possible props.

<img src="Images/Dungeon_06.PNG" alt="Generate props and their type." width="300"/>

### Models

Last part is placement of models to create the dungeon. This means placing floor and ceiling on any non-empty tile and placing walls around halls and rooms and doorframes in the right place.

It also means the placement of props, which are selected picked from list of possible props of desired type.

<img src="Images/Dungeon_07.PNG" alt="Placed models." width="300"/>

### What next?

This if FAR from finished, but it will have to do for now. 
So what changes could be done next?

#### Rooms

The rooms shouldn't be generated like the halls. Better aproach would be to create including models as a prefabs and better distribute prop locations in them. Current aproach allows only placing one prop directly on tile, which limits what is possible. Also rotation of props can either be non existent or random, but both can create some weird outcomes.

Also original plan ment to create entrance at the top and exit at the bottom of the dungeon. With large doors in each. But that was scrapped with the reduction to single floor :(.

#### Floors

The 3d aspect of the dungeon that had to be scrapped for now.

## Game: Slime Puncher

Due to time constraint (and the fact I had to make many things from scratch, due to my own choices) the final game is bit simple. You go floor by floor killing slimes until slimes kill you. While playing a score in form of number of killed slimes (including current max). When player is killed he is returned to begining.


Final product can be seen here: https://trampod.itch.io/slime-puncher

### Slimes

Main part of Slime Puncher are the slimes. Their design originated from lazyness to learn how to bake navmesh at runtime. So I created a small round blob that jumps around and if it "sees" player it tries to lung itself against him (and by doing so dealing damage).

#### Technicalities

All their animation were done using tweening (for the wobbling on contact) and physical simulation for movement. AI is realy simple (which I feel fitting for a mindless slime). It randomly jumps around, so it is not stationary. If it sees player in it's "vision" range it will start jumping towards him. If it sees player in it's attack range, it will lung itself straight against player face (or at least generaly in that direction).

#### PCG: Stats

Slimes have three stats that are randomly generated. Speed, resistance and strength. Speed governs jump force, resistance governs health and strength governs both damage and also increases attack jump force. These stats are not generated completely random, to make sure there are no OP or useless slimes. It picks two stats that are increased and the third is left at the lowest possible value. Then then the selected stats get random value between 0 and 1 and after that these values are normalized, so every slime has sum of stats equal to 1.

#### PCG: Looks that kill

To distinguish what are the stats of slime they directly affect how slime looks. Each stat affects the slime in different way so each combination is distinct.

<img src="Images/Slime_02.PNG" alt="Placed models." width="300"/>


- Resistance scales the size of slime. The more durable it is, the bigger it gets.
- Strength shifts color toward red color. Since it shifts from green to red, it goes thrue orange and brown.
- Speed could be taken as the base state since it is shifting color toward green (original base color for the slimes).

<img src="Images/Slime_01.PNG" alt="Placed models." width="300"/>


During development there were other ideas how to distinguish diferent stats. Original being simple mapping of stat to one of the rgb channels. But that resulted in less appealing visuals, so it had to be scratched. Trying to solve this also caused setting one of the stats to minimum, and it was not changed back.

Another idea was tried and quickly abandoned. Giving the slimes core, so they feel more whole. Didn't like it so didn't stay.

#### Pointless blob

When they hit non-player and non-slime objects they leave behind a small blob in their color. Does nothing, but looks cool.

<img src="Images/Slime_04.PNG" alt="Placed models." width="300"/>


#### Alternative enemies

One other idea for enemy was a ghost that would be immune to attacks and that would be able to go thrue walls. Only way to kill him would be to destroy his core crystal, which he would be orbiting with some distance. This was also created based on my lazyness to deal with navmesh.

And then there was a wague idea for traps or some fire spitting stationary enemy. Again so I wouldn't have to deal with navigation.

In the end I didn't have time for them and decided to focuse on the slimes, which are more cute anyway. But I still like that my lazyness brought me to ideas that I would probably not have and instead opted for some generic skeletons or something.

### Character

I had no plans to deal with complicated characters and rigging or animations. So I decided I will just use capsules and tweening. Body is a capsule that is never seen (only in shadow) and hand is another capsule that can be seen (except the shadow).

<img src="Images/Slime_03.PNG" alt="Placed models." width="300"/>


Originaly there were ment to be multiple weapons (held in the hand), but I didn't have time.

#### Fighting

Hand is strong and damages all enemies in it's path while punching. Enemies are knocked back a little bit.

### Props

Since original props were just temporary proof of concept (terrible concept, but that's topic for dungeon section of this project) the props were reworked to be more interesting.

<img src="Images/Slime_05.PNG" alt="Placed models." width="300"/>

They are more put together and also have random orientation (random rotation with 90° steps).

<img src="Images/Slime_06.PNG" alt="Placed models." width="300"/>


### Main menu

I didn't have time to learn how to make UI, so I had to go around the problem. I didn't want to throw player into action. No that is not a good solution, which I learned at a recent gamejam. But I don't know how to make UI menu so that would be unpredictable if I succeed. So I created something I do know how to make... a scene.

I created a new scene which contained a cell in which player starts and a bridge with a portal. There is clear way the player is supposed to go. And to top it off he is also shown how to get to the next level, since I use the same portal inside the dungeon.

<img src="Images/Slime_07.PNG" alt="Placed models." width="300"/>

When player dies he is returned to the same cell, so he is already familiar with the situation.

### Audio

Every sound effect was personaly recorded (all three of them) and for music was used my proceduraly generated album [Certifiable Library](https://soundcloud.com/trampod/sets/certifiable-library).

### Groundwork

As with the dungeon generation this part was also intended to be more sophisticated... but isn't.

But there are multiple places where it is prepared for future expansion. Most notable are weapons, since the current code could be used for a simple slashing weapon, that also uses tweening for attack animation.

But this game probably won't be expanded from the point it is now. Instead I will probably create new game based on knowledge I gained here to make something better.



